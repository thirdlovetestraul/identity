'use strict';
const mongoose = require('mongoose');
mongoose.Promise = require('bluebird');
const Schema = mongoose.Schema;

const ProfileSchema = new Schema({
  email: { type: String },
  googleId: { type: String },
  displayName: { type: String },
  gender: { type: String },
  firstName: { type: String },
  lastName: { type: String }
});

ProfileSchema.index({ email: 1, score: -1 });

module.exports = mongoose.model('Profile', ProfileSchema);