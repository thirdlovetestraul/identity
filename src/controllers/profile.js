const logger = require('../lib/logger')
const Profile = require('../models/Profile');

module.exports.getProfile = (profileId) => {
  return Profile.findById(profileId)
}
