const googleController = require('../service/passport');
const GoogleStrategy = require('passport-google-oauth').OAuth2Strategy;
const logger = require('../lib/logger');
const config = require('../config');
const Profile = require('../models/Profile');

googleController.use(new GoogleStrategy({
    clientID: config.google.clientID,
    clientSecret: config.google.clientSecret,
    callbackURL: config.callbackHost + "/identity/auth/google/callback",
    proxy: true,
  },
  (token, tokenSecret, profile, done) => {
    try {
      const email = profile.emails[0].value

      Profile.findOne({ email: email })
        .then(existingProfile => {
          if (existingProfile) {
            return done(null, existingProfile._id.toString());
          }
          const newProfile = new Profile({
                                  email: email,
                                  googleId: profile.id,
                                  displayName: profile.displayName,
                                  gender: profile.gender,
                                  firstName: "",
                                  lastName: ""});
          newProfile.save()
            .then( () => {
              done(null, newProfile._id.toString());
            }).catch(error => {
              logger.error('Error processing profile from google ' + error);
            })
        })
        .catch(error => {
          logger.error('Error processing profile from google ' + error);
        })
    } catch (error) {
        logger.error('Generic Error processing profile from google  ' + error);
    }
  }
));

googleController.getProfile = (profileId) => {
  console.log(profileId)
  return Profile.findById(profileId)
}

module.exports = googleController