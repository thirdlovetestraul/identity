'use strict';
const exceptions = require('./exceptions')

var getError = function(status, message, code, aboutLink) {
  return {
    status: status,
    message: message,
    code: code,
    link: {
      about: aboutLink
    }
  }
}

var errorHandler = function () {
  return function (err, req, res, next) {

    switch (err.constructor) {
      case exceptions.Unauthorized:
      res.status(401)
      .json(getError(401, 
                        err.message, 
                        err.code,
                        err.aboutLink));
      break; 
      case exceptions.MissingArgument:
        res.status(400);
        res.json(getError(400, 
                          err.message, 
                          err.code,
                          err.aboutLink));
        break; 
      default:
        res.status(500);
        res.json(getError(500, 
                          err.message, 
                          err.code ? err.code : "5000",
                          err.aboutLink ? err.aboutLink : "http://www.domain.com/about/5000"));
    }
  };
};

module.exports = errorHandler;