'use strict';

module.exports.MissingArgument = class InvalidArgumentType {
    constructor(message) {
        this.message = message;
        this.code = 4000;
        this.aboutLink = "http://www.domain.com/about/4000";
    }
}

module.exports.MissingArgument = class MissingArgument {
    constructor(message) {
        this.message = message;
        this.code = 4001;
        this.aboutLink = "http://www.domain.com/about/4001";
    }
}

module.exports.Unauthorized = class Unauthorized {
    constructor(message) {
        this.message = message;
        this.code = 4001;
        this.aboutLink = "http://www.domain.com/about/4001";
    }
}
