/**
 * configuration file
 *
 */

'use strict';

var _ = require('lodash');

var config = (function (env) {
  var conf = {};

  // Common configuration
  conf.common =  {
    app: {
      name: "rest-express"
    },
    port: process.env.PORT || 3000,
    mongoURI: process.env.MONGO_URI || "mongodb://root:example@127.0.0.1:27017/identity?authSource=admin",
    callbackHost: process.env.CALLBACK_HOST || "http://localhost:8080",
    google: {
      clientID: process.env.COOGLE_CLIENT_ID || "560912966239-6ksm1de26q86vkjejmt5kkogbv5o4drl.apps.googleusercontent.com",
      clientSecret: process.env.COOGLE_CLIENT_SECRET || "qSYX6ALuX_UX5Nmnmleyznfl"
    }
  };

  // Development configuration
  conf.development = {
    env: "development",
  };

  // Test configuration
  conf.test = {
    env: "test",
    port: process.env.PORT || 3030,
  };

  // Production configuration
  conf.production = {
    env: "production",
  };

  return _.merge(conf.common, conf[env]);

})(process.env.NODE_ENV || 'development');

module.exports = config;