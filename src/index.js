
var mongoose = require('mongoose');
var config = require('./config');
var logger = require('./lib/logger');
var app = require('./app');

app.set('port', config.port);

// start listening for requests
app.listen(app.get('port'));
logger.info(config.app.name + ' listening on port ' + app.get('port') + ' for ' + config.env);
