const logger = require('../lib/logger');
const express = require('express');

logger.debug('configuring routes');

const auth = require('./auth');
const router = express.Router();

// API versions
router.use('/identity/auth', auth);

module.exports = router;
