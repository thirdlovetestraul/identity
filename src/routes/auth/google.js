const exceptions = require('../../lib/exceptions')
const express = require('express');
const googleController = require('../../controllers/google')
const router = express.Router();


router.get('/', (req, res, next) => {
  if (!req.query.redirect_uri) {
    throw new exceptions.MissingArgument("redirect_uri query parameter is required")
  }
  googleController.authenticate('google', { scope: ['profile', 'email'], state: req.query.redirect_uri })(req, res);
})

router.get('/callback', googleController.authenticate('google'), (req, res) => {
  googleController.getProfile(req.user)
    .then(existingProfile => {
      let jwtToken = googleController.genJWTToken(existingProfile);
      
      res.redirect(`${req.query.state}?access_token=${jwtToken}&token_type=bearer&expires_in=3600`);
    })
    .catch(error => {
      res.redirect(`${req.query.state}?error=${error}`);
    })
});

module.exports = router;
