const exceptions = require('../../lib/exceptions')
const passport = require('../../service/passport')
const express = require('express');
const profileController = require('../../controllers/profile')
const router = express.Router();

router.get('/', (req, res, next) => {
  profileController.getProfile(req.profileId)
    .then(profile => {
      res.send({
        data: profile
      })
    })
    .catch(error => {
      throw new exceptions.MissingArgument("Invalid token")
    })
})

module.exports = router;
