const express = require('express');
const router = express.Router();

const google = require('./google.js');
const profile = require('./profile.js');

router.use('/google', google);
router.use('/profile/me', profile);

module.exports = router;
