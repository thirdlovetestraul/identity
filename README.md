# Identity Service 

Steps to create docker package:

```
docker build . -t identity
docker run -p 3000:3000 -e PORT=3000 -e CALLBACK_HOST=http://localhost:8080 -it identity
```

Steps to push package:

```
docker login
docker tag identity rrobledo/identity
docker push rrobledo/identity
```
